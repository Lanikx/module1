﻿using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            int a = 4, b = 5;

            Module1 mod = new Module1();
            mod.SwapItems(a, b);
            int[] testArray = { 1, 4, 7, 3, 0, -4, 6, 8 };
            mod.GetMinimumValue(testArray);
        }

        public int[] SwapItems(int a, int b)
        {
            (a, b) = (b, a);
            return new int[] { a, b };
        }

        public int GetMinimumValue(int[] input)
        {
            return input.Min();
        }
    }
}
